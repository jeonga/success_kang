from django.apps import AppConfig


class AdminEquipmentHistoryConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'admin_equipment_history'
